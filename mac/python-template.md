# Python Virtual Environment tool combinations

## Direnv + pyenv + virtualenv

Direnv automatically initializes an environment for your terminal whenever a `.envrc` file is detected in the current path. This can be used in conjunction with pyenv and virtualenv to automatically activate a python virtual environment whenever you `cd` into a directory.

##### Install Tools

    brew install direnv
    brew install pyenv
    pip install virtualenv

##### Set up PIP to require a virtual environment (optional)

Add to `~/.bashrc` to require pip to have an activated virtual environment

    export PIP_REQUIRE_VIRTUALENV=true
    
    
Add to `~/.bashrc` to allow global installations using `gpip2` or `gpip3`

    gpip3(){
       PIP_REQUIRE_VIRTUALENV="" pip3 "$@"
    }
    
    gpip2(){
       PIP_REQUIRE_VIRTUALENV="" pip2 "$@"
    }
    
##### Configure direnv

Add to `~/.bashrc` to initialize direnv

    eval "$(direnv hook bash)"

    show_virtual_env() {
      if [ -n "$VIRTUAL_ENV" ]; then
        local PROJECT="$(echo $VIRTUAL_ENV | rev | cut -d '/' -f 3 | rev)"
        echo "($PROJECT) "
      fi
    }
    
    PS1='$(show_virtual_env)'$PS1
    
Add to `~/.direnvrc` to allow using pyenv versions

    use_python() {
      local python_root=$HOME/.pyenv/versions/$1
      load_prefix "$python_root"
      layout python "$python_root/bin/python"
    }
    
##### Install some python versions in pyenv

    pyenv install 3.4.7
    pyenv install 3.5.4
    pyenv install 3.6.3
    
##### Test it out

Create a new folder 

Create `.envrc` in the folder and add the following

    use python 3.5.4
    
Initialize direnv. `direnv allow` only has to be called when `.envrc` is modified

    cd [folder name] 
    direnv allow

Notice that your prompt now begins with the project directory name to indicate the activated virtual environment

Check the python version

    python --version
    
Your new virtual environment is stored in `.direnv/`

**note:** If you move a directory with a direnv virtual environment, you should `rm -rf .direnv` and then reinstall your pip dependencies.
