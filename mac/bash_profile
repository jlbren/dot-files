#
# ~/.bashrc
#
alias batchme='sh /Users/jon.brenner/dev/bioinf-software/apps/bioinf-batchme/batchme/launch.sh'
# Mac ports path
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# pyenv path
export PATH=/Users/jon.brenner/.pyenv/bin:$PATH
# path to pip installs
export PATH=/opt/local/Library/Frameworks/Python.framework/Versions/3.6/bin:$PATH
# path to gnu ports utils
export PATH=/opt/local/libexec/gnubin/:$PATH
# Use GNU coreutils instead of BSD
#PATH="/usr/local/opt/coreutils/libexec/gnubin:/usr/local/cellar:$PATH"
# MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
# If not running interactively, don't do anything
[[ $- != *i* ]] && return
# Add color to ls
alias ls='ls --color=auto'
# export PS1="\[\e[35m\][\[\e[m\]\[\e[36m\]\W\[\e[m\]\[\e[35m\]]\[\e[m\]🌹 "
export PS1="\[\e[35m\][\[\e[m\]\[\e[36m\]\W\[\e[m\]\[\e[35m\]]\[\e[m\] \[\e[32m\]\`parse_git_branch\`\[\e[m\]\n\[\e[36m\]\\$\[\e[m\] "
# PS1='[\u@\h \W]\$ '
# Arch: hook to search repos for missing command
#source /usr/share/doc/pkgfile/command-not-found.bash
# Auto cd on entering path
# TODO
# shopt -s autocd
# Alias for sourcing bashrc
alias refresh='source ~/.bash_profile'
# must press ctrl-D 2+1 times to exit shell
export IGNOREEOF="2"

# Tab completion settings
# Ignore caps
bind "set completion-ignore-case on"
# Allow fuzzy search for _ and -
bind "set completion-map-case on"
# Show all matches if ambiguous
bind "set show-all-if-ambiguous on"
# Color by file type when showing completions (uses LS_COLORS)
set colored-stats on

#Better history stuff
# Append to the history file, don't overwrite it
shopt -s histappend
# Save multi-line commands as one command
shopt -s cmdhist
# Record each line as it gets issued
PROMPT_COMMAND='history -a'
# Make history size BIG, neglible performance overhead
HISTSIZE=50000
HISTFILESIZE=10000
# Avoid duplicate entries
HISTCONTROL="erasedups:ignoreboth"
# Don't record insignificant commands
export HISTIGNORE="&:[ ]*:exit:ls:bg:fg:history"
# Useful timestamp format
HISTTIMEFORMAT='%F %T '
# cat multiline commands into one line in history file
shopt -s cmdhist
# log commands immediately, not just on close (small overhead)
PROMPT_COMMAND='history -a'
# fix simple spelling errors for tab completion and cd
# TODO
# shopt -s dirspell
shopt -s cdspell

# extend cd path so that it looks in the home if not found in current
CDPATH=".:~/"

# Useful aliases.
# List whats taking up the most space.
alias diskhog="du -S | sort -n -r |more"
# Show me the size (sorted) of only the folders in this directory
alias folders="find . -maxdepth 1 -type d -print | xargs du -sk | sort -rn"
# ls aliases
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias lt='ls -laptr'  # oldest first sort
# cd into the old directory
alias bd='cd "$OLDPWD"'
# tail logs
alias logs="find /var/log -type f -exec file {} \; | grep 'text' | cut -d' ' -f1 | sed -e's/:$//g' | grep -v '[0-9]$' | xargs tail -f"
# home dir
alias home="/Users/jon.brenner"
# functions!
# go up n dirs
up() { cd $(eval printf '../'%.0s {1..$1}) && pwd; }

psgrep() {
	if [ ! -z $1 ] ; then
		echo "Grepping for processes matching $1..."
		ps aux | grep $1 | grep -v grep
	else
		echo "!! Need name to grep for"
	fi
}

# set LS colors
# test -e ~/.config/dircolors.txt && \
#    eval `dircolors -b ~/.config/dircolors.txt`
# FIXME eval $(dircolors ~/.config/dircolors.txt)

# Python modules
# export PYTHONPATH="${HOME}/dev/pyensembl:${PYTHONPATH}"

# PIP in virtualenvs
export PIP_REQUIRE_VIRTUALENV=true
export PIP_DOWNLOAD_CACHE=${HOME}/.pip/cache
syspip2() {
          PIP_REQUIRE_VIRTUALENV="" pip2 "$@"
}

syspip3() {
	    PIP_REQUIRE_VIRTUALENV="" pip "$@"
}

export -f syspip3
export -f syspip2

# Figure out which config bash is sourcing...
export BASH_CONF="profile"

# get current branch in git repo
function parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		echo "[${BRANCH}]"
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}


# Git tab complete
# source ~/.git-completion.bash
# bash-completion
if [ -f /opt/local/etc/profile.d/bash_completion.sh ]; then
  . /opt/local/etc/profile.d/bash_completion.sh
fi

# direnv
eval "$(direnv hook bash)"

# show virenv before prompt
show_virtual_env() {
	if [ -n "$VIRTUAL_ENV" ]; then
    local PROJECT="$(echo $VIRTUAL_ENV | rev | cut -d '/' -f 3 | rev)"
    echo "($PROJECT) "
  fi
}
# snake if in virtualenv
venv_snek() {
  if [ -n "$VIRTUAL_ENV" ]; then
    echo "🐍 "
  fi
}
PS1='$(venv_snek)'$PS1

# coffee if in node env
node_env() {
  if [ -n "$NODE" ]; then
    echo "☕ "
  fi
}
PS1='$(node_env)'$PS1


# python startup
export PYTHONSTARTUP=$HOME/.pythonrc.py

alias envprod='source ~/scripts/env.prod.sh'
alias envstaging='source ~/scripts/env.staging.sh'
alias dbconnect='sh ~/scripts/db_connect.sh'

 noblack() {
     $(PRE_COMMIT_ALLOW_NO_CONFIG=1 $*)
 }

 alias tm='yokadi'

 #function get_interop_data() {
 #   set -x
 #   aws s3 cp --recursive --exclude=* --include=*.xml --include=InterOp/ s3://$1 .
 #   set +x
 #}

export PATH="$HOME/.cargo/bin:$PATH"

# path to neotree for emacs
export NEOTREE="$HOME/repos/neotree"
